﻿using AssignmentProject1.Utils;
using OpenQA.Selenium;

namespace AssignmentProject1.Pages
{
   public class VicRoadsPermit: LoginPage
    {
    
        public static IWebElement vhcleType => driver.FindElement(By.CssSelector
                                      ("select#ph_pagebody_0_phthreecolumnmaincontent_0_panel_VehicleType_DDList"));
        public static IWebElement passngrVhclType => driver.FindElement(By.CssSelector
                                           ("select#ph_pagebody_0_phthreecolumnmaincontent_0_panel_PassengerVehicleSubType_DDList"));
        public static IWebElement vhclAddress => driver.FindElement(By.CssSelector
                                           ("input#ph_pagebody_0_phthreecolumnmaincontent_0_panel_AddressLine_SingleLine_CtrlHolderDivShown"));
        public static IWebElement vhclAddressOption => driver.FindElement
                                      (By.CssSelector("//div[text()='14 Lancewood Rd, MANOR LAKES VIC 3024']"));

  
        public static IWebElement permitDuration => driver.FindElement(By.CssSelector
                                       ("select#ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitDuration_DDList"));

        public static  IWebElement btnNext => driver.FindElement(By.CssSelector
                                            ("INPUT#ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnNext"));


        public static IWebElement step2 => driver.FindElement(By.XPath("//p[contains(text(),' Select permit type')]"));

        public static IWebElement permitStartDate => driver.FindElement(By.CssSelector
                                                    ("input#ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitStartDate_Date"));

        public static void SelectVehicle(string vehicleType)
        {
           
           ElementUtils.SelectValueFromDropDown(driver, vhcleType, vehicleType);
        }
        public static void SelectPassengerVehicle(string PVehicleType)
        {
            ElementUtils.SelectValueFromDropDown(driver, passngrVhclType, PVehicleType);
        }

        public static void EnterGarageAddress(string textToSend)
        {
            ElementUtils.SendText(driver, vhclAddress, textToSend);
        }
        public static void SelectPermitDuration(string pDuration)
        {
            ElementUtils.SelectValueFromDropDown(driver, permitDuration, pDuration); 
        }
        public static void ClickNextButton()
        {
            ElementUtils.ClickElement(driver, btnNext);
        }
        public static void VerifyTextDisplayed()
        {
            ElementUtils.IsElementDisplayed(driver, step2);
        }
       // public static void GetVehicleType(System.Data.DataSet dataset, string colName)
        //{
          //  var details = dataset.Tables["Sheet1"];
           // var vehicle = details.GetRowValue(colName);
            //Console.Write(vehicle);
       // }

        
    }
}
