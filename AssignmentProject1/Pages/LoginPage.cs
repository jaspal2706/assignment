﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using AssignmentProject1.Utils;
using NUnit.Framework;

namespace AssignmentProject1.Pages
{
    public class LoginPage
    {
        public static IWebDriver driver { get; set; }

        
        public LoginPage()

        {
            // browser is hard coded, it can be acheived from run Test.settings or from some other config file such as json
            string browser = "chrome";

            if (browser.ToLower() == "chrome")
            {
                driver = new ChromeDriver();
            }
            else if (browser.ToLower() == "firefox")
            {
                //driver for firefox initialise here
                
            }
            else
            {
             Console.Write("Driver not found");
            }

            driver.Manage().Window.Maximize();
        }

   

        [TearDown]
        public void TearDown()
        {
            Console.WriteLine("testing");
            driver.Quit();
            driver = null;
        }
       

    }
}
