﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;

namespace AssignmentProject1.Utils
{
    
    public abstract class SeleniumPage
    {
        public IWebDriver driver { get; }

        protected SeleniumPage(IWebDriver Driver)
        {
            driver = Driver;
        }
    }
}
