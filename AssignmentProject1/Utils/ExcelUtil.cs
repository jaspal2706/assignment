﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using xl = Microsoft.Office.Interop.Excel;
namespace AssignmentProject1.Utils
{

    class ExcelUtil
    {
        xl.Application xlApp = null;
        xl.Workbooks workbooks = null;
        xl.Workbook workbook = null;
        Hashtable sheets;
        public string xlFilePath;
        public ExcelUtil(string xlFilePath)
        {
            this.xlFilePath = xlFilePath;
        }

        /// <summary>
        /// Open excel file for reading
        /// </summary>
        public void OpenExcel()
        {
            xlApp = new xl.Application();
            workbooks = xlApp.Workbooks;
            workbook = workbooks.Open(xlFilePath);
            sheets = new Hashtable();
            int count = 1;
            foreach (xl.Worksheet sheet in workbook.Sheets)
            {
                sheets[count] = sheet.Name;
                count++;
            }

        }

        /// <summary>
        /// Closes the excel
        /// </summary>
        public void CloseExcel()
        {
            workbook.Close(false, xlFilePath);
            Marshal.FinalReleaseComObject(workbook);
            workbook = null;

            workbooks.Close();
            Marshal.FinalReleaseComObject(workbooks);
            workbooks = null;

            xlApp.Quit();
            Marshal.FinalReleaseComObject(xlApp);
            xlApp = null;
        }

        /// <summary>
        /// Return cell value using column number annd row number
        /// </summary>
        /// <param name="sheetName">Name of the sheet to read</param>
        /// <param name="columnNumber">Cell Column number</param>
        /// <param name="rowNumber">Cell row number</param>
        /// <returns></returns>
        public string GetCellData(string sheetName, int columnNumber, int rowNumber)
        {
            OpenExcel();
            string value = string.Empty;
            int sheetValue = 0;
            if (sheets.ContainsValue(sheetName))
            {
                foreach (DictionaryEntry sheet in sheets)
                {
                    if (sheet.Value.Equals(sheetName))
                    {
                        sheetValue = (int)sheet.Key;
                    }
                }
                //xl.Worksheet worksheet= null;
                xl.Worksheet worksheet = workbook.Worksheets[sheetValue] as xl.Worksheet;
                xl.Range range = worksheet.UsedRange;
                value = Convert.ToString((range.Cells[rowNumber, columnNumber] as xl.Range).Value2);
                Marshal.FinalReleaseComObject(worksheet);
                worksheet = null;


            }
            CloseExcel();
            return value;

        }

        /// <summary>
        /// Return value of cell using row number and column name
        /// </summary>
        /// <param name="sheetName">Name of sheet to read</param>
        /// <param name="columName">Name of column to read</param>
        /// <param name="rowNumber">Row number of cell to read</param>
        /// <returns></returns>
        public string GetCellDataByColumnName(string sheetName, string columName, int rowNumber)
        {
            OpenExcel();
            string value = string.Empty;
            int sheetValue = 0;
            int columnNumber = 0;
            if (sheets.ContainsValue(sheetName))
            {
                foreach (DictionaryEntry sheet in sheets)
                {
                    if (sheet.Value.Equals(sheetName))
                    {
                        sheetValue = (int)sheet.Key;
                    }
                }
                //xl.Worksheet worksheet= null;
                xl.Worksheet worksheet = workbook.Worksheets[sheetValue] as xl.Worksheet;
                xl.Range range = worksheet.UsedRange;

                for (int i = 1; i <= range.Columns.Count; i++)
                {
                    string columnValue = Convert.ToString((range.Cells[1, i] as xl.Range).Value2);
                    if (columName.ToLower() == columnValue.ToLower())
                    {
                        columnNumber = i;
                        break;
                    }

                }
                value = Convert.ToString((range.Cells[rowNumber, columnNumber] as xl.Range).Value2);

            }
            CloseExcel();
            return value;

        }
    }
}