﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AssignmentProject1.Utils
{
    class ElementUtils
    {
             
        public static TimeSpan PollingInterval { get; private set; }

        /// <summary>
        /// wait until element is clickable then click the element
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="element"></param>
        public static void ClickElement(IWebDriver driver, IWebElement webElement)
        {
            WaitUntilElementToBeClickable(driver, webElement);
            webElement.Click();
        }

        ///<summary>
        /// Wait for element to be clickable
        /// </summary>
        /// <param name="driver">The driver is the instance of the opened driver.</param>
        /// <param name="webElement">The webelement to wait for</param>
        ///<param name="description">Description of the webelement</param>
      
   
        public static void SendText(IWebDriver driver, IWebElement webElement, string textToSend)
        {
            WaitUntilElementToBeClickable(driver, webElement);
            webElement.SendKeys(textToSend);
        }
        public static string GetText(IWebDriver driver, IWebElement webElement)
        {
            WaitUntilElementToBeClickable(driver, webElement);
            return webElement.Text;
        }
        public static void WaitUntilElementToBeClickable(IWebDriver driver, IWebElement webElement)
        {
            //LogUtil.LogMessage("Debug", "Wait for the element '" + description + "' to be clickable");
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            try
            {
                wait.Until(ExpectedConditions.ElementToBeClickable(webElement));
            }
            catch (NoSuchElementException e)
            {
                //LogUtil.LogMessage("Error", "Error in WaitUntilElementToBeClickable Function", e);
            }
            catch (Exception e)
            {
                // LogUtil.LogMessage("Error", "Error in WaitUntilElementToBeClickable Function", e);
            }
        }
       /// <summary>
        /// Wait for element to be present and load
        /// </summary>
        /// <param name="driver">The driver is the instance of the opened driver.</param>
        /// <param name="webElement">The webelement to wait for</param>
        /// <param name="description">Description of the webelement</param>
        public static void WaitUntilElementIsPresent(IWebDriver driver, IWebElement webElement, string description = "NOT MENTIONED")
        {
            
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            {
                PollingInterval = TimeSpan.FromSeconds(5);
            };
            wait.Until(drv => webElement);
        }
        public static void SelectValueFromDropDown(IWebDriver driver, IWebElement parentWebelement, string drpDwntext)
        {
            WaitUntilElementIsPresent(driver, parentWebelement);
            var selectElement = new SelectElement(parentWebelement);
            selectElement.SelectByText(drpDwntext);
        }

        public static bool IsElementDisplayed(IWebDriver driver, IWebElement webelement)
        {
            WaitUntilElementIsPresent(driver, webelement);
            return webelement.Displayed;
            
        }

        /// <summary>
        /// Wait for all elements of page to be visible
        /// </summary>
        /// <param name="driver">The driver is the instance of the opened driver.</param>
        /// <param name="byType">define if the locator type is - Id, class, cssSelector or TagName</param>
        /// <param name="locator">Locator value</param>
        /// <param name="description">Description of the webelement</param>
        public static void WaitVisibilityOfAllElements(IWebDriver driver, string byType, string locator, string description = "NOT MENTIONED")
        {
           
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));

            switch (byType.ToLower())
            {
                case "id":
                    wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.Id(locator)));
                    break;
                case "class":
                    wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.ClassName(locator)));
                    break;
                case "cssselector":
                    wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.CssSelector(locator)));
                    break;
                case "tagname":
                    wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.TagName(locator)));
                    break;
                case "xpath":
                    wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath(locator)));
                    break;
                default:
                  //  LogUtil.LogMessage("Error", "Invalid byType");
                    break;
            }

        }

    }
}
