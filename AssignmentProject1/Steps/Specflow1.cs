﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AssignmentProject1.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace AssignmentProject1.Steps
{
    [Binding]
    public class Specflow1: LoginPage
    {
        public Specflow1(): base()
        {
        }
        //VicRoadsPermit vicroads = new VicRoadsPermit(driver);

            
       
        [Given(@"I launch the vic roads")]
        public void GivenILaunchTheVicRoads()
        {
        
            string url = "https://www.vicroads.vic.gov.au/registration/limited-use-permits/unregistered-vehicle-permits/get-an-unregistered-vehicle-permit";
            driver.Navigate().GoToUrl(url);
           
        }

        [Then(@"I select (.*) from vehicle typr drop down")]
        public void ThenISelectFromVehicleTyprDropDown(string vehicleType)
        {
            VicRoadsPermit.SelectVehicle(vehicleType);
       
        }

        [Then(@"I select (.*) passenger vehicle type from drop down")]
        public void ThenISelectPassengerVehicleTypeFromDropDown(string PvehicleType)
        {
            VicRoadsPermit.SelectPassengerVehicle(PvehicleType);
        }

        [Then(@"I enter (.*) in the address field for garage")]
        public void ThenIEnterInTheAddressFieldForGarage(string garageAddress)
        {
            VicRoadsPermit.EnterGarageAddress(garageAddress);

        }

        [Then(@"I select (.*) duration form the drop down")]
        public void ThenISelectDurationFormTheDropDown(string permitDuration)
        {
            VicRoadsPermit.SelectPermitDuration(permitDuration);
        }

        [Then(@"I click on next button")]
        public void ThenIClickOnNextButton()
        {
            VicRoadsPermit.ClickNextButton();
        }

        [Then(@"I verify permit type text is visible")]
        public void ThenIVerifyPermitTypeTextIsVisible()
        {
            VicRoadsPermit.VerifyTextDisplayed();
            
        }




    }
}
