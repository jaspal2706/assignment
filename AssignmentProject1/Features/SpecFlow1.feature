﻿Feature: SpecFlow1
	Get an Unregistered Vehicle Permit

@GetVehiclePermit
Scenario Outline: Get an Unregistered Vehicle Permit
	Given I launch the vic roads
	Then I select <Vehicle_Type> from vehicle typr drop down
	And I select <passenger_vehicle_type> passenger vehicle type from drop down
	And I enter <customer_address> in the address field for garage
	And I select <permit> duration form the drop down
	And I click on next button
	And I verify permit type text is visible

	Examples:
		| Vehicle_Type      | passenger_vehicle_type | customer_address                      | permit |
		| Passenger vehicle | Coupe                  | 14 Lancewood Rd, MANOR LAKES VIC 3024 | 2 days |