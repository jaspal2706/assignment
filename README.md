# Assignment Project for Interview

The purpose of this project is to launch the Vicroads website, Navigate to required page, select values for different required fields and Verify if the user is able to move to Step-2.

## Small description of project

The given solution is achieved my making use of Nunit, Speflow  and C#. Along with this, some other packages were also added to the project.

The POM project is divided primarily into 4 steps:


```bash
Feature- Contains Gerkin/Specflow (SpecFlow1.Feature in this case)
Steps- contains Step definition (Specflow1.cs )
Page- Contains Page related methods and objects
Utils- contains commong utility methods

```

## Running the test
In order to run the test case, please open the solution in visual studio, select the SpecFlow1.Feature  from feature folder and then run the test case

## Note : 
Current solution is reading the data from the table withing the test case. The solution with reading data from excel could not be achieved as I am using Ubuntu (Linux) machine, which I giving error while importing the excel plugin in VS code.
